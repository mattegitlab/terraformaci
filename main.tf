# ACI

provider "aci" {
  username = var.APIC_USERNAME
  password = var.APIC_PASSWORD
  url      = var.APIC_HOSTNAME
  insecure = var.APIC_INSECURE
}
resource "aci_vpc_explicit_protection_group" "vpc" {
  for_each = {for vpc_int, record in var.vpc : vpc_int => record}
   name  = each.value["name"]
   switch1 = each.value["switch1"]
   switch2 = each.value["switch2"]
   vpc_explicit_protection_group_id  = each.value["group_id"]
}

resource "aci_vlan_pool" "vlan_pool" {
  for_each = {for vlan_pol, record in var.vlan_p : vlan_pol => record}
    name = each.value["name"]
    alloc_mode  = each.value["alloc_mode"]
}

resource "aci_ranges" "vlan_pols" {
  for_each = {for vlan_pols, record in var.vlan_ps : vlan_pols => record} 
    vlan_pool_dn = each.value["vlan_pool_dn"]
    to = each.value["to"]
    from = each.value["from"]
    role = each.value["role"]
    depends_on = [aci_vlan_pool.vlan_pool]
}

resource "aci_physical_domain" "physd" {
  for_each = {for physd_pol, record in var.physd : physd_pol => record}
    name = each.value["name"]
    relation_infra_rs_vlan_ns = each.value["vlan_pool_dn"]
}

resource "aci_l3_domain_profile" "l3_dom" {
  for_each = {for l3dom_pol, record in var.l3dom : l3dom_pol => record}
    name = each.value["name"]
    relation_infra_rs_vlan_ns = each.value["vlan_pool_dn"]
    depends_on = [aci_physical_domain.physd]
}

resource "aci_attachable_access_entity_profile" "aep" {
  for_each = {for aep_pol, record in var.aep : aep_pol => record}
    name = each.value["name"]
    relation_infra_rs_dom_p = toset([each.value["relation_infra_rs_dom_p"]])
    depends_on = [aci_physical_domain.physd,aci_l3_domain_profile.l3_dom]    
}

resource "aci_lldp_interface_policy" "lldp_int" {
  for_each = {for lldp_int, record in var.lldp : lldp_int => record}
    name = each.value["name"]
    admin_rx_st = each.value["admin_rx_st"]
    admin_tx_st = each.value["admin_tx_st"]
} 
resource "aci_cdp_interface_policy" "cdp_int" {
  for_each = {for cdp_int, record in var.cdp : cdp_int => record}
    name = each.value["name"]
    admin_st = each.value["admin_st"]
} 
resource "aci_miscabling_protocol_interface_policy" "mcp_int" {
  for_each = {for mcp_int, record in var.mcp : mcp_int => record}
    name = each.value["name"]
    admin_st = each.value["admin_st"]
} 
resource "aci_fabric_if_pol" "ll" {
  for_each = {for ll_int, record in var.ll : ll_int => record}
    name = each.value["name"]
    auto_neg = each.value["auto_neg"]
    fec_mode = each.value["fec_mode"]
    speed = each.value["speed"]
} 
resource "aci_lacp_policy" "lacp" {
  for_each = {for lacp_int, record in var.lacp : lacp_int => record}
    name = each.value["name"]
    ctrl = each.value["ctrl"]
    max_links = each.value["max_links"]
    min_links = each.value["min_links"]
    mode = each.value["mode"]
} 
resource "aci_leaf_access_port_policy_group" "lap" {
  for_each = {for lap_int, record in var.lap : lap_int => record}
    name                            = each.value["name"]
    relation_infra_rs_cdp_if_pol    = each.value["relation_infra_rs_cdp_if_pol"]
    relation_infra_rs_lldp_if_pol   = each.value["relation_infra_rs_lldp_if_pol"]
    relation_infra_rs_mcp_if_pol    = each.value["relation_infra_rs_mcp_if_pol"]
    relation_infra_rs_att_ent_p     = each.value["relation_infra_rs_att_ent_p"]
    relation_infra_rs_h_if_pol      = each.value["relation_infra_rs_h_if_pol"]
    depends_on = [aci_lldp_interface_policy.lldp_int,aci_fabric_if_pol.ll,aci_miscabling_protocol_interface_policy.mcp_int,aci_attachable_access_entity_profile.aep]
}
resource "aci_spine_port_policy_group" "sap" {
  for_each = {for sap_int, record in var.sap : sap_int => record}
    name                            = each.value["name"]
    relation_infra_rs_cdp_if_pol    = each.value["relation_infra_rs_cdp_if_pol"]
    relation_infra_rs_att_ent_p     = each.value["relation_infra_rs_att_ent_p"]
    relation_infra_rs_h_if_pol      = each.value["relation_infra_rs_h_if_pol"]
    depends_on = [aci_fabric_if_pol.ll,aci_attachable_access_entity_profile.aep]
}

resource "aci_leaf_access_bundle_policy_group" "vpc_int" {
  for_each = {for vpcint, record in var.vpc_int : vpcint => record}
    name                            = each.value["name"]
    lag_t                           = each.value["lag_t"]
    relation_infra_rs_cdp_if_pol    = each.value["relation_infra_rs_cdp_if_pol"]
    relation_infra_rs_lldp_if_pol   = each.value["relation_infra_rs_lldp_if_pol"]
    relation_infra_rs_mcp_if_pol    = each.value["relation_infra_rs_mcp_if_pol"]
    relation_infra_rs_att_ent_p     = each.value["relation_infra_rs_att_ent_p"]
    relation_infra_rs_lacp_pol      = each.value["relation_infra_rs_lacp_pol"]
    relation_infra_rs_h_if_pol      = each.value["relation_infra_rs_h_if_pol"]
    depends_on = [aci_lldp_interface_policy.lldp_int,aci_fabric_if_pol.ll,aci_miscabling_protocol_interface_policy.mcp_int,aci_attachable_access_entity_profile.aep]
}
resource "aci_leaf_interface_profile" "leaf_intpro" {
  for_each = {for leaf_int, record in var.leaf : leaf_int => record}
    name                            = each.value["name"]
  depends_on = [aci_leaf_access_bundle_policy_group.vpc_int,aci_leaf_access_port_policy_group.lap]
}
resource "aci_access_port_selector" "aps" {
  for_each = {for apsint, record in var.aps : apsint => record}
    leaf_interface_profile_dn      = each.value["leaf_interface_profile_dn"]
    name                           = each.value["name"]
    access_port_selector_type      = each.value["access_port_selector_type"]
    relation_infra_rs_acc_base_grp = each.value["relation_infra_rs_acc_base_grp"]
  depends_on = [aci_leaf_interface_profile.leaf_intpro]
}
resource "aci_access_port_block" "aps_blk" {
  for_each = {for apsint_pol, record in var.aps_int : apsint_pol => record}
    access_port_selector_dn = each.value["access_port_selector_dn"]
    name                    = each.value["name"]
    from_card               = each.value["from_card"]
    from_port               = each.value["from_port"]
    to_card                 = each.value["to_card"]
    to_port                 = each.value["to_port"]
  depends_on = [aci_access_port_selector.aps]
}
resource "aci_leaf_profile" "swpro" {
  for_each = {for swipro_pol, record in var.swipro : swipro_pol => record}
    name                          = each.value["name"]
    relation_infra_rs_acc_port_p  = toset([each.value["port_p"]])
    leaf_selector {
      name                        = each.value["swi_sel_name"]
      switch_association_type     = each.value["type"]
      node_block {
        name                      = each.value["blk_name"]
        from_                     = each.value["from"]
        to_                       = each.value["to"]
        }
      }
  depends_on = [aci_leaf_interface_profile.leaf_intpro]
}
resource "aci_spine_interface_profile" "spine_intpro" {
  for_each = {for spine_int, record in var.spine : spine_int => record}
    name                            = each.value["name"]
  depends_on = [aci_spine_port_policy_group.sap]
}
resource "aci_spine_port_selector" "sps" {
  for_each = {for spsint, record in var.sps : spsint => record}
    spine_profile_dn              = each.value["spine_profile_dn"]
    tdn                           = each.value["tdn"]
  depends_on = [aci_spine_profile.sppro]  
}
resource "aci_spine_profile" "sppro" {
  for_each = {for spipro_pol, record in var.spipro : spipro_pol => record}
    name                          = each.value["name"]
    relation_infra_rs_sp_acc_port_p  = toset([each.value["port_spi"]])
  depends_on = [aci_spine_interface_profile.spine_intpro]  
}
resource "aci_spine_switch_association" "spass" {
  for_each = {for spiasspro_pol, record in var.spass : spiasspro_pol => record}
    name                          = each.value["name"]
    spine_profile_dn              = each.value["sp_prof_dn"]
    spine_switch_association_type     = each.value["spine_type"]
  depends_on = [aci_spine_profile.sppro]
}