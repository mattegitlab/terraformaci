variable "APIC_USERNAME" {
  type    = string
  default = "admin"
}
variable "APIC_PASSWORD" {
  type    = string
  default = "C15co123"
}
variable "APIC_HOSTNAME" {
  type    = string
  default = "https://10.50.137.206"
}
variable "APIC_INSECURE" {
  type    = string
  default = "true"
}
variable "vpc" {
  description = "Configure the VPC interface settings"
  type        = map
  default = {
    vpc-1 = {
      "name"  = "VPC_DOM_101_L101_L102"
      "switch1" = "101"
      "switch2" = "102"
      "group_id"  = "301"
    }
  }
}

variable "vlan_p" {
  description = "Configure the VLAN Pools interface settings"
  type        = map
  default = {
    vp-1 = {
      "name"  = "HXV_UCS_INFRA_VLANP"
      "alloc_mode" = "static"    
    }
    vp-2 = {
      "name"  = "L3DOM_VLANP"
      "alloc_mode" = "static"    
    }
    vp-3 = {
      "name"  = "MSO_VLANP"
      "alloc_mode" = "static"    
    }
  }
}

variable "vlan_ps" {
  description = "Configure the VLAN Pools vlans settings"
  type        = map
  default = {
      vp1 = {
        "vlan_pool_dn"  = "uni/infra/vlanns-[HXV_UCS_INFRA_VLANP]-static"
        "to"  = "vlan-1253"
        "alloc_mode"  = "static"
        "from"  = "vlan-1252"
        "role"  = "external"
      }
      vp2 = {
        "vlan_pool_dn"  = "uni/infra/vlanns-[HXV_UCS_INFRA_VLANP]-static"
        "to"  = "vlan-1010"
        "alloc_mode"  = "static"
        "from"  = "vlan-1010"
        "role"  = "external"
      }
      vp3 = {
        "vlan_pool_dn"  = "uni/infra/vlanns-[HXV_UCS_INFRA_VLANP]-static"
        "to"  = "vlan-1011"
        "alloc_mode"  = "static"
        "from"  = "vlan-1011"
        "role"  = "external"
      }
      vp4 = {
        "vlan_pool_dn"  = "uni/infra/vlanns-[L3DOM_VLANP]-static"
        "to"  = "vlan-12"
        "alloc_mode"  = "static"
        "from"  = "vlan-12"
        "role"  = "external"
      }
      vp5 = {
        "vlan_pool_dn"  = "uni/infra/vlanns-[MSO_VLANP]-static"
        "to"  = "vlan-4"
        "alloc_mode"  = "static"
        "from"  = "vlan-4"
        "role"  = "external"
      }     
  }
}

variable "l3dom" {
    description = "Configure the L3dom settings"
    type        = map
    default = {
      l3d1 = {
        "name"  = "EXTERNAL_L3DOM"
        "vlan_pool_dn"  = "uni/infra/vlanns-[L3DOM_VLANP]-static"
      }
      l3d2 = {
        "name"  = "MSO_L3DOM"
        "vlan_pool_dn"  = "uni/infra/vlanns-[MSO_VLANP]-static"
      }
    }
}

variable "physd" {
    description = "Configure the Phys Dom settings"
    type        = map
    default = {
      phyd1 = {
        "name"  = "HXV_UCS_INFRA_PHYD"
        "vlan_pool_dn"  = "uni/infra/vlanns-[HXV_UCS_INFRA_VLANP]-static"
      }
    }
}

variable "aep" {
    description = "Configure the AEP"
    type        = map
    default = {
      aep1 = {
        "name"  = "HXV_UCS_INFRA_aep"
        "relation_infra_rs_dom_p" = "uni/phys-HXV_UCS_INFRA_PHYD"
      }
      aep2 = {
        "name"  = "MSO_aep"
        "relation_infra_rs_dom_p" = "uni/l3dom-MSO_L3DOM" 
      }
      aep3 = {
        "name"  = "L3NET_aep"
        "relation_infra_rs_dom_p" = "uni/l3dom-EXTERNAL_L3DOM" 
      }
    }
}

variable "lldp" {
  description = "Configure the LLDP interface settings"
  type        = map
  default = {
    lldp-on = {
      "name"  = "LLDP_ON"
      "admin_rx_st" = "enabled"
      "admin_tx_st" = "enabled" 
    }
    lldp-off = {
      "name"  = "LLDP_OFF"
      "admin_rx_st" = "disabled"
      "admin_tx_st" = "disabled"
    }
  }
}

variable "mcp" {
  description = "Configure the mcp interface settings"
  type        = map
  default = {
    mcp-on = {
      "name"  = "MCP_ON"
      "admin_st" = "enabled"
    }
    mcp-off = {
      "name"  = "MCP_OFF"
      "admin_st" = "disabled"
    }
  }
}
variable "cdp" {
  description = "Configure the cdp interface settings"
  type        = map
  default = {
    cdp-on = {
      "name"  = "CDP_ON"
      "admin_st" = "enabled"
    }
    cdp-off = {
      "name"  = "CDP_OFF"
      "admin_st" = "disabled"
    }
  }
}

variable "ll" {
  description = "Configure the Link Layer interface settings"
  type        = map
  default = {
    l_10g_auto = {
     "name"        = "L_10G_AUTO"
     "auto_neg"    = "on"
     "fec_mode"    = "inherit"
     "speed"       = "10G"
    }
    l_100g_auto = {
     "name"        = "L_100G_AUTO"
     "auto_neg"    = "on"
     "fec_mode"    = "inherit"
     "speed"       = "100G"
    }
    l_25g_auto = {
     "name"        = "L_25G_AUTO"
     "auto_neg"    = "on"
     "fec_mode"    = "inherit"
     "speed"       = "25G"
    }
    l_40g_auto = {
     "name"        = "L_40G_AUTO"
     "auto_neg"    = "on"
     "fec_mode"    = "inherit"
     "speed"       = "40G"
    }
    l_400g_auto = {
     "name"        = "L_400G_AUTO"
     "auto_neg"    = "on"
     "fec_mode"    = "inherit"
     "speed"       = "400G"
    }
  }
}
variable "lacp" {
  description = "Configure the LACP interface settings"
  type        = map
  default = {
    mac = {
     "name"        = "MAC_PINNING"
     "ctrl"        = ["susp-individual", "graceful-conv", "fast-sel-hot-stdby"]
     "max_links"   = "16"
     "min_links"   = "1"
     "mode"        = "mac-pin"
    }
    static = {
     "name"        = "STATIC_PC"
     "ctrl"        = ["susp-individual", "graceful-conv", "fast-sel-hot-stdby"]
     "max_links"   = "16"
     "min_links"   = "1"
     "mode"        = "off"
    }
    lacp = {
     "name"        = "LACP_ACT"
     "ctrl"        = ["susp-individual", "load-defer", "graceful-conv", "fast-sel-hot-stdby"]
     "max_links"   = "16"
     "min_links"   = "1"
     "mode"        = "active"
    }
  }
}
variable "lap" {
  description = "Configure the Access Port interface settings"
  type        = map
  default = {
    lap1 = {
        "name"                            = "L3NET_POLGRP"
        "relation_infra_rs_cdp_if_pol"    = "uni/infra/cdpIfP-CDP_ON"
        "relation_infra_rs_lldp_if_pol"   = "uni/infra/lldpIfP-LLDP_ON"
        "relation_infra_rs_mcp_if_pol"    = "uni/infra/mcpIfP-MCP_OFF"
        "relation_infra_rs_att_ent_p"     = "uni/infra/attentp-L3NET_aep"
        "relation_infra_rs_h_if_pol"      = "uni/infra/hintfpol-L_10G_AUTO"
    }
    lap2 = {
        "name"                            = "HXV_UCS_INFRA_POLGRP"
        "relation_infra_rs_cdp_if_pol"    = "uni/infra/cdpIfP-CDP_ON"
        "relation_infra_rs_lldp_if_pol"   = "uni/infra/lldpIfP-LLDP_ON"
        "relation_infra_rs_mcp_if_pol"    = "uni/infra/mcpIfP-MCP_OFF"
        "relation_infra_rs_att_ent_p"     = "uni/infra/attentp-HXV_UCS_INFRA_aep"
        "relation_infra_rs_h_if_pol"      = "uni/infra/hintfpol-L_10G_AUTO"
    }
  }
}
variable "sap" {
  description = "Configure the Spine Port interface settings"
  type        = map
  default = {
    sap1 = {
        "name"                            = "MSO_POLGRP"
        "relation_infra_rs_cdp_if_pol"    = "uni/infra/cdpIfP-CDP_ON"
        "relation_infra_rs_att_ent_p"     = "uni/infra/attentp-MSO_aep"
        "relation_infra_rs_h_if_pol"      = "uni/infra/hintfpol-L_40G_AUTO"
    }
  }
}
variable "vpc_int" {
  description = "Configure the VPC interface settings"
  type        = map
  default = {
    vpc1 = {
        "name"                            = "HXV_UCS_VPC_INFRA_POLGRP"
        "lag_t"                           = "node"
        "relation_infra_rs_cdp_if_pol"    = "uni/infra/cdpIfP-CDP_ON"
        "relation_infra_rs_lldp_if_pol"   = "uni/infra/lldpIfP-LLDP_ON"
        "relation_infra_rs_mcp_if_pol"    = "uni/infra/mcpIfP-MCP_OFF"
        "relation_infra_rs_att_ent_p"     = "uni/infra/attentp-HXV_UCS_INFRA_aep"
        "relation_infra_rs_lacp_pol"      = "uni/infra/lacplagp-LACP_ACT"
        "relation_infra_rs_h_if_pol"      = "uni/infra/hintfpol-L_10G_AUTO"
    }
  }
}
variable "leaf" {
    description = "Configure the leaf interface profile settings"
    type        = map
    default = {
        leafip1 = {
            "name"        = "LEAF_101_INTPRO"
         }
        leafip2 = {
            "name"        = "LEAF_102_INTPRO"
        }
        leafip3 = {
            "name"        = "LEAF_101_102_INTPRO"
        }
    }
}
variable "spine" {
    description = "Configure the spine interface profile settings"
    type        = map
    default = {
        spine1 = {
            "name"        = "SPINE_201_INTPRO"
         }
    }
}

variable "aps" {
    description = "Configure the leaf access ports settings"
    type        = map
    default = {
        aps1 = {
            "leaf_interface_profile_dn"      = "uni/infra/accportprof-LEAF_101_102_INTPRO"
            "name"                           = "ETH1:48"
            "access_port_selector_type"      = "range"
            "relation_infra_rs_acc_base_grp" = "uni/infra/funcprof/accportgrp-L3NET_POLGRP"
        }
        aps2 = {
            "leaf_interface_profile_dn"      = "uni/infra/accportprof-LEAF_101_102_INTPRO"
            "name"                           = "ETH1:2"
            "access_port_selector_type"      = "range"
            "relation_infra_rs_acc_base_grp" = "uni/infra/funcprof/accportgrp-HXV_UCS_INFRA_POLGRP"
        }
        aps3 = {
            "leaf_interface_profile_dn"      = "uni/infra/accportprof-LEAF_101_102_INTPRO"
            "name"                           = "ETH1:4"
            "access_port_selector_type"      = "range"
            "relation_infra_rs_acc_base_grp" = "uni/infra/funcprof/accbundle-HXV_UCS_VPC_INFRA_POLGRP"
        }
    }
}
variable "aps_int" {
    description = "Configure the leaf access interface ports settings"
    type        = map
    default = {
        aps_int1 = {
            "access_port_selector_dn" = "uni/infra/accportprof-LEAF_101_102_INTPRO/hports-ETH1:48-typ-range"
            "name"                    = "ETH1:48"
            "from_card"               = "1"
            "from_port"               = "48"
            "to_card"                 = "1"
            "to_port"                 = "48"
        }
        aps_int2 = {
            "access_port_selector_dn" = "uni/infra/accportprof-LEAF_101_102_INTPRO/hports-ETH1:2-typ-range"
            "name"                    = "ETH1:2"
            "from_card"               = "1"
            "from_port"               = "2"
            "to_card"                 = "1"
            "to_port"                 = "2"
        }
    }
}
variable swipro {
    description = "Configure the leaf switch profile settings"
    type        = map
    default = {
        swipro1 = {
            "name"                              = "LEAF_101_SWPRO"
            "port_p"      = "uni/infra/accportprof-LEAF_101_INTPRO"
            "swi_sel_name"                      = "L101_SWSEL"
            "type"                              = "range"
            "blk_name"                          = "blk1"
            "from"                              = "101"
            "to"                                = "101"
            }
        swipro2 = {
            "name"                              = "LEAF_102_SWPRO"
            "port_p"      = "uni/infra/accportprof-LEAF_102_INTPRO"
            "swi_sel_name"                      = "L102_SWSEL"
            "type"                              = "range"
            "blk_name"                          = "blk1"
            "from"                              = "102"
            "to"                                = "102"
            }
        swipro3 = {
            "name"                              = "LEAF_101_102_SWPRO"
            "port_p"      = "uni/infra/accportprof-LEAF_101_102_INTPRO"
            "swi_sel_name"                      = "L101_102_SWSEL"
            "type"                              = "range"
            "blk_name"                          = "blk1"
            "from"                              = "101"
            "to"                                = "102"
            }
    }
}
variable "sps" {
    description = "Configure the spine ports settings"
    type        = map
    default = {
        sps1 = {
            "spine_profile_dn"              = "uni/infra/spprof-SPINE_201_SWPRO"
            "tdn"                           = "uni/infra/spaccportprof-SPINE_201_INTPRO"
        }
    }
}
variable "sps_int" {
    description = "Configure the spine interface ports settings"
    type        = map
    default = {
        sps_int1 = {
            "access_port_selector_dn" = "uni/infra/accportprof-SPINE_201_INTPRO/shports-ETH1:31-typ-range"
            "name"                    = "ETH1:31"
            "from_card"               = "1"
            "from_port"               = "31"
            "to_card"                 = "1"
            "to_port"                 = "31"
        }
    }
}
variable spass {
    description = "Configure the spine switch association settings"
    type        = map
    default = {
        spass1 = {
            "name"                              = "SPINE_201_SWSEL"
            "sp_prof_dn"                        = "uni/infra/spprof-SPINE_201_SWPRO" 
            "spine_type"                        = "range"
            }
    }
}
variable spipro {
    description = "Configure the spine switch profile settings"
    type        = map
    default = {
        spipro1 = {
            "name"                              = "SPINE_201_SWPRO"
            "port_spi"                         = "uni/infra/spaccportprof-SPINE_201_INTPRO"
            }
    }
}